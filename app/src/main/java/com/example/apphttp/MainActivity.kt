package com.example.apphttp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URI
import java.net.URL

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnRed = findViewById<Button>(R.id.btnRed)
        val btnHttp = findViewById<Button>(R.id.btnSolicitudHttp)

        btnRed.setOnClickListener {
            //codigo para validar la red
            if(Network.hayRed(this)){
                Toast.makeText(this, "Si Hay Red", Toast.LENGTH_LONG).show()
            }else{
                Toast.makeText(this, "No Hay Red", Toast.LENGTH_LONG).show()
            }
        }
        btnHttp.setOnClickListener{
            if(Network.hayRed(this)){
                Log.d("________________", descargarDatos("http://www.facebook.com   "))
               //   descargarDatos()
            }else{
                Toast.makeText(this, "No Hay Red", Toast.LENGTH_LONG).show()
            }
        }
    }

    @Throws(IOException::class)
    private fun descargarDatos(url:String):String{

        var inputStream:InputStream? = null
        try {
            val url = URL(url)
            val conn = url.openConnection() as HttpURLConnection
            conn.requestMethod = "GET"
            conn.connect()

            inputStream = conn.inputStream
            return inputStream.bufferedReader().use {
                it.readText()
            }
        }finally {
            if (inputStream != null){
                inputStream.close()
            }
        }
    }
}
